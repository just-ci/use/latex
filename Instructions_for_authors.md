# Instructions for authors

- A pdf file can be obtained with the command `make` (on system where "make" utility is installed -- by default on Linux distributions).
- Please use macros and aliases (in preamble.tex) as much as possible, and check that macros aren't already defined before defining a new one.

If you are using git for version control and/or to collaborate on a document,

- *Please*: get updated versions of the project with `git pull --rebase`.
  This will ensure that no different branches and merges are created in case of conflicts, giving a clean log tree.
- *Please please*: write one sentence per line. This will ensure that differences between versions are easily visible.
