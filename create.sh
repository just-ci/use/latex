#!/bin/bash
# TNO, Department Applied Cryptography and Quantum Algorithms
# Script to fetch the Just CI LaTeX template

LOCAL_INSTALL=false
DIR=$(pwd)
INSTRUCTIONS=false
CICD=false
GITIGNORE=false
MAKEFILE=false

# To keep the code as simple as possible (so that it can be executed even by a
# simple shell like sh), the help message is hard-coded.
# Therefore, any modification to the parser requires some manual adjuting of
# this message as well.
HELP_MESSAGE="
\nThis shell script fetches relevant files for a LaTeX template and write them
in the directory indicated by the user.
\nIt assumes Unix-directory format.

\n\nThe following parameters can be given as input:

\n\n -h | --help
\n		Prints this help message and exits.

\n\n-d=[directory] | --d=[directory]
\n		Writes files in the given directory.

\n\n-l | --local
\n		Attempts to fetch the files from the current directory, instead
of looking for them on gitlab.

\n\n-i | --instructions
\n		Adds instructions for authors as well.


\n\n-c | --cicd
\n		Adds .gitlab-ci.yml as well.

\n\n-g | --gitignore
\n		Adds .gitignore as well.

\n\n-m | --makefile
\n		Adds Makefile as well.

\n\n-f | --full
\n		Adds all optional files.

\n\nNotice that parameters must be passed separately to the script, e.g.
'./create_latex_project.sh -la' will not work.
"

# Argument parser.
# Not written as separate function to preserve compatibility with sh
for arg in "$@"; do
	case $arg in
	-h | --help)
		echo $HELP_MESSAGE
		exit
		;;
	-l | --local)
		LOCAL_INSTALL=true
		;;
	-d=* | --directory=*)
		DIR="${arg#*=}"
		;;
	-i | --instructions)
		INSTRUCTIONS=true
		;;
	-c | --cicd)
		CICD=true
		;;
	-g | --gitignore)
		GITIGNORE=true
		;;
	-m | --makefile)
		MAKEFILE=true
		;;
	-f | --full)
		CICD=true
		INSTRUCTIONS=true
		GITIGNORE=true
		MAKEFILE=true
		;;
	*)
		echo "Invalid parameter"
		;;
	esac
done

if [ ! -d "$DIR" ]; then
	echo "The given directory does not exist, aborting"
	exit
else
	echo "Adding LaTeX files"
	if $LOCAL_INSTALL; then
		cp ./main.tex "$DIR/main.tex"
		cp ./preamble.tex "$DIR/preamble.tex"
	else
		curl -fsSL https://gitlab.com/just-ci/use/latex/-/raw/main/main.tex -s -o "$DIR/main.tex"
		curl -fsSL https://gitlab.com/just-ci/use/latex/-/raw/main/preamble.tex -s -o "$DIR/preamble.tex"
	fi

	if $INSTRUCTIONS; then
		echo "Adding instructions for authors"
		if $LOCAL_INSTALL; then
			cp ./Instructions_for_authors.md "$DIR/Instructions_for_authors.md"
		else
			curl -fsSL https://gitlab.com/just-ci/use/latex/-/raw/main/Instructions_for_authors.md -s -o "$DIR/Instructions_for_authors.md"
		fi
	fi

	if $CICD; then
		echo "Adding CI/CD file"
		if $LOCAL_INSTALL; then
			cp ./.gitlab-ci.yml "$DIR/.gitlab-ci.yml"
		else
			curl -fsSL https://gitlab.com/just-ci/use/latex/-/raw/main/.gitlab-ci.yml -s -o "$DIR/.gitlab-ci.yml"
		fi
	fi

	if $GITIGNORE; then
		echo "Adding gitignore"
		if $LOCAL_INSTALL; then
			cp ./.gitignore "$DIR/.gitignore"
		else
			curl -fsSL https://gitlab.com/just-ci/use/latex/-/raw/main/.gitignore -s -o "$DIR/.gitignore"
		fi
	fi

	if $MAKEFILE; then
		echo "Adding Makefile"
		if $LOCAL_INSTALL; then
			cp ./Makefile "$DIR/Makefile"
		else
			curl -fsSL https://gitlab.com/just-ci/use/latex/-/raw/main/Makefile -s -o "$DIR/Makefile"
		fi
	fi
fi
