# LaTeX template

This repository contains a basic LaTeX template with the following properties and extra files:

1. A preamble with relevant package imports and macros, specifically meant for mathematics/computer-science documents.
2. A Makefile for easy compilation (either locally or on supported remote CI/CD pipelines).
3. Instructions for authors, meant to help in collaboratively work with LaTeX documents via git.
4. A Gitlab CI/CD file, for automatic file compilation on Gitlab servers.
5. A script to fetch the relevant files.


## Usage instructions

### tl;dr

Run the following command from your project directory:

```shell
curl -fsSL https://gitlab.com/just-ci/use/latex/-/raw/main/create.sh | sh
```

Or, if you want to include all optional files,

```shell
curl -fsSL https://gitlab.com/just-ci/use/latex/-/raw/main/create.sh | sh -s -- -f
```

### Detailed instructions

The `main.tex` file, together with `preamble.tex`, serves as a basis document template.

The Makefile allows users to simply run the `make` command to compile the LaTeX file into PDF (assuming make is installed).

The Gitlab CI file allows for remote compilation on the Gitlab server.

Finally, the shell script `create.sh` allows users to copy all desired files to a given location, without the need to clone the repository or download it beforehand (although this option is also provided).
The easiest way to use it is to fetch it via curl (or similar commands) and then feed it to a shell, e.g. by running the following command in your project directory:

```shell
curl -fsSL https://gitlab.com/just-ci/use/latex/-/raw/main/create.sh | sh
```
Notice that by default, this will only download the .tex files.
If you want to download the optional files as well, you can do so by adding `-s -- [parameters]` to your command, e.g. `curl -fsSL https://gitlab.com/just-ci/use/latex/-/raw/main/create.sh | sh -s -- -f`.
The complete list of commands is as follows:


| Flag                                  | Effect                                                                                         |
| ------------------------------------- | ---------------------------------------------------------------------------------------------- |
| `-h` or `--help`                      | Prints a help message and exits, no files will be dowloaded.                                   |
| `-d=[directory]` or `--d=[directory]` | Writes files in the given directory.                                                           |
| `-l` or `--local`                     | Attempts to fetch the files from the current directory, instead of looking for them on Gitlab. |
| `-i` or `--instructions`              | Adds instructions for authors as well.                                                         |
| `-c` or `--cicd`                      | Adds .gitlab-ci.yml as well.                                                                   |
| `-g` or `--gitignore`                 | Adds .gitignore as well.                                                                       |
| `-m` or `--makefile`                  | Adds Makefile as well.                                                                         |
| `-f` or `--full`                      | Adds all optional files.                                                                       |

Notice that parameters must be passed separately to the script, e.g. `./create.sh -li` will not work.
